package com.gmail.jd4656.customcrates;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main extends JavaPlugin {
    static Main plugin;
    static List<String> crateLocations = new ArrayList<String>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        getConfig().addDefault("crates", new ArrayList<Location>());
        getConfig().options().copyDefaults(true);
        saveConfig();

        if (getConfig().getList("crates") != null) {
            crateLocations = getConfig().getStringList("crates");
        }

        this.getCommand("customcrate").setExecutor(new CommandCrate());
        getServer().getPluginManager().registerEvents(new EventListeners(), this);

        BukkitTask task = new BukkitRunnable() {
            public void run() {
                HashMap<String, String> changes = new HashMap<>();
                for (String key : Main.crateLocations) {
                    String[] splitStr = key.split(":");
                    String curKey = key;
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    String worldName = splitStr[0];
                    String tierName = splitStr[5];
                    long time = 0;

                    try {
                        x = Integer.parseInt(splitStr[1]);
                        y = Integer.parseInt(splitStr[2]);
                        z = Integer.parseInt(splitStr[3]);
                        time = Long.parseLong(splitStr[4]);
                    } catch (NumberFormatException ignored){};

                    if ((System.currentTimeMillis() - time) >= ((10 * 60 * 1000)) && splitStr[6].equals("0")) {
                        changes.put(curKey, worldName + ":" + x + ":" + y + ":" + z + ":" + time + ":" + tierName + ":1");
                        Bukkit.broadcastMessage(ChatColor.BOLD.toString() + ChatColor.DARK_RED + "A crate at x: " + x + " y: " + y + " z: " + z +
                                " in world " + ChatColor.RED + ChatColor.BOLD.toString() + worldName + ChatColor.DARK_RED + " has still not been found!");
                    }
                }

                if (changes.size() > 0) {
                    for (Map.Entry<String, String> entry : changes.entrySet()) {
                        Main.crateLocations.remove(entry.getKey());
                        Main.crateLocations.add(entry.getValue());
                        Main.plugin.getConfig().set("crates", Main.crateLocations);
                        Main.plugin.saveConfig();
                    }
                    changes.clear();
                }
            }
        }.runTaskTimer(this, 0, 1 * 60 * (1 * 20));

        plugin.getLogger().info("CustomCrates loaded.");
    }
}
