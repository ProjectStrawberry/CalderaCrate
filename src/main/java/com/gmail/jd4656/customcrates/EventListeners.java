package com.gmail.jd4656.customcrates;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class EventListeners implements Listener {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        for (String key : Main.crateLocations) {
            String[] splitStr = key.split(":");

            int x = 0;
            int y = 0;
            int z = 0;

            String worldName = splitStr[0];

            try {
                x = Integer.parseInt(splitStr[1]);
                y = Integer.parseInt(splitStr[2]);
                z = Integer.parseInt(splitStr[3]);
            } catch (NumberFormatException ignored){};

            if (splitStr[6].equals("0")) {
                player.sendMessage(ChatColor.DARK_RED + ChatColor.BOLD.toString() + "There is a crate somewhere near x:" + Math.round((float) x / 100f) * 100
                        + " y: " + y + " z: " + Math.round((float) z / 100f) * 100 + " in world " + ChatColor.RED + worldName);
            } else {
                player.sendMessage(ChatColor.DARK_RED + ChatColor.BOLD.toString() + "There is a crate at x: " + Math.round(x) + " y: " + y + " z: " + z +
                    " in world " + ChatColor.RED + worldName);
            }
        }
    }
    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (block == null) return;
        if (block.getState() == null) return;
        if (!(block.getState() instanceof Chest)) return;
        if (Main.crateLocations.size() < 1) return;

        String mapKey = "";
        boolean foundLocation = false;

        String tierName = "";
        String worldName = "";
        int x = 0;
        int y = 0;
        int z = 0;

        for (String key : Main.crateLocations) {
            String[] splitStr = key.split(":");
            mapKey = key;
            worldName = splitStr[0];
            tierName = splitStr[5];
            x = 0;
            y = 0;
            z = 0;

            try {
                x = Integer.parseInt(splitStr[1]);
                y = Integer.parseInt(splitStr[2]);
                z = Integer.parseInt(splitStr[3]);
            } catch (NumberFormatException ignored){};

            if (block.getX() == x && block.getY() == y && block.getZ() == z) {
                foundLocation = true;
                break;
            }
        }

        if (!foundLocation) return;

        Main.crateLocations.remove(mapKey);
        Main.plugin.getConfig().set("crates", Main.crateLocations);
        Main.plugin.saveConfig();

        event.setCancelled(true);
        Chest chest = (Chest) block.getState();
        ItemStack[] chestItems = chest.getBlockInventory().getContents();

        boolean droppedItems = false;

        for (ItemStack curItem : chestItems) {
            if (curItem == null) continue;
            HashMap excess = player.getInventory().addItem(curItem);
            if (!excess.isEmpty()) {
                for (Object entry : excess.values()) {
                    player.getWorld().dropItem(player.getLocation().clone().add(0, 1, 0), (ItemStack) entry);
                    droppedItems = true;
                }
            }
        }

        chest.getBlockInventory().clear();
        block.setType(Material.AIR);

        player.sendMessage(ChatColor.GOLD + "You've claimed a " + ChatColor.RED + tierName + ChatColor.GOLD + " crate.");
        if (droppedItems) player.sendMessage(ChatColor.GOLD + "Your inventory did not have enough space for all of the items. Some items have been dropped at your feet.");

        Bukkit.broadcastMessage(ChatColor.DARK_RED + ChatColor.BOLD.toString() + "The crate at x: " + x + " y: " + y + " z: " + z +
                " in world " + ChatColor.RED + worldName + ChatColor.DARK_RED + ChatColor.BOLD.toString() + " has been found by " + player.getDisplayName());
    }
}
