package com.gmail.jd4656.customcrates;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import com.wimbli.WorldBorder.BorderData;
import com.wimbli.WorldBorder.Config;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class CommandCrate implements TabExecutor {
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("customcrates.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GREEN + "CustomCrate commands:");
            sender.sendMessage(ChatColor.GOLD + "/customcrate create tier <tiername> - Creates a tier.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate create crate <cratename> <tiername> - Creates a crate in the specified tier.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate listtiers - Lists all of the tiers added.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate add <cratename> - Adds the item you're holding to the crate.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate delete <tiername> <cratename> - Deletes a crate.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate deleteitem <tiername> <cratename> - Opens a GUI to remove an item from a crate.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate drop <tiername> - Drops a random crate from this tier.");
            sender.sendMessage(ChatColor.GOLD + "/customcrate drop random - Drops a random crate from a random tier.");
            return true;
        }

        if (args[0].equals("drop")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate drop <tiername|random> <world> - Drops a crate somewhere in the world.");
                return true;
            }
            String tierName = args[1];

            if (Main.plugin.getConfig().get("tiers") == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no tiers added.");
                return true;
            }

            String worldName;
            if (args.length < 3) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You need to specify a world name to use this command from the console.");
                    return true;
                }
                worldName = ((Player) sender).getWorld().getName();
            } else {
                worldName = args[2];
            }

            World world = Bukkit.getWorld(worldName);

            if (world == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That world does not exist.");
                return true;
            }

            if (tierName.equals("random")) {
                Set<String> tiers = Main.plugin.getConfig().getConfigurationSection("tiers").getKeys(false);

                int size = tiers.size();
                int item = new Random().nextInt(size);
                int i = 0;

                for(String obj : tiers)
                {
                    if (i == item) {
                        tierName = obj;
                    }
                    i++;
                }
            }

            if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                return true;
            }

            if (Main.plugin.getConfig().getConfigurationSection("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier has no crates added.");
                return true;
            }

            Set<String> crates = Main.plugin.getConfig().getConfigurationSection("tiers." + tierName).getKeys(false);

            if (crates.size() < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no crates added in this tier.");
                return true;
            }

            int size = crates.size();
            int item = new Random().nextInt(size);
            int i = 0;

            String randomCrate = "";

            for(String obj : crates) {
                if (i == item) {
                    randomCrate = obj;
                }
                i++;
            }

            BorderData border = Config.Border(worldName);
            if (border != null) {
                Location center = new Location(world, border.getX(), 64, border.getZ());
                Random rand = new Random();
                double angle = rand.nextDouble() * 360;
                double x = center.getX() + (rand.nextDouble() * border.getRadiusX() * Math.cos(Math.toRadians(angle)));
                double z = center.getZ() + (rand.nextDouble() * border.getRadiusZ() * Math.sin(Math.toRadians(angle)));

                double y = world.getHighestBlockYAt((int) x, (int) z);
                Location crateLocation = new Location(world, x, y, z);
                Particle part
                world.spawnParticle();

                Block block = crateLocation.getBlock();
                block.setType(Material.CHEST);

                if (block.getState() instanceof Chest) {
                    Chest chest = (Chest) block.getState();
                    Inventory chestInv = chest.getInventory();

                    List<ItemStack> items = (List<ItemStack>) Main.plugin.getConfig().getList("tiers." + tierName + "."  + randomCrate);

                    chestInv.addItem(items.toArray(new ItemStack[items.size()]));

                    Main.crateLocations.add(worldName + ":" + crateLocation.getBlockX() + ":" + crateLocation.getBlockY() + ":" + crateLocation.getBlockZ() + ":" + System.currentTimeMillis() + ":" + tierName + ":0");
                    Main.plugin.getConfig().set("crates", Main.crateLocations);
                    Main.plugin.saveConfig();

                    sender.sendMessage(ChatColor.GOLD + "A " + tierName + " crate has been spawned at x: " + Math.round(crateLocation.getX()) + " y: " + Math.round(crateLocation.getY())
                            + " z: " + Math.round(crateLocation.getZ()) + " in world " + ChatColor.RED + worldName);
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + ChatColor.BOLD.toString() + "A " + ChatColor.GREEN + ChatColor.BOLD.toString() +  tierName + ChatColor.DARK_RED + ChatColor.BOLD.toString() + " crate has spawned somewhere near x: " + Math.round((float) crateLocation.getX() / 100f) * 100
                            + " y: " + crateLocation.getBlockY() + " z: " + Math.round((float) crateLocation.getZ() / 100f) * 100 + " in world " + ChatColor.RED + ChatColor.BOLD.toString() + worldName);

                    return true;
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please set a border with WorldBorder before dropping crates.");
                return true;
            }
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        Player player = (Player) sender;

        if (args[0].equals("listtiers")) {
            if (Main.plugin.getConfig().get("tiers") == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no tiers added.");
                return true;
            }

            Set<String> tiers = Main.plugin.getConfig().getConfigurationSection("tiers").getKeys(false);
            if (tiers.size() < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no tiers added.");
                return true;
            }
            String tierOutput = "";
            boolean first = true;
            for (String tier : tiers) {
                if (first) {
                    tierOutput += ChatColor.RED + tier + ChatColor.GOLD;
                    first = false;
                    continue;
                }
                tierOutput += ", " + ChatColor.RED + tier + ChatColor.GOLD;
            }

            sender.sendMessage(ChatColor.GOLD + "Tiers: " + tierOutput);
            return true;
        }

        if (args[0].equals("listcrates")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate listcrates <tiername>");
                return true;
            }
            String tierName = args[1];
            if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                return true;
            }

            if (Main.plugin.getConfig().getConfigurationSection("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier has no crates added.");
                return true;
            }

            Set<String> crates = Main.plugin.getConfig().getConfigurationSection("tiers." + tierName).getKeys(false);
            if (crates.size() < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no crates added in this tier.");
                return true;
            }
            String crateOutput = "";
            boolean first = true;
            for (String crate : crates) {
                if (first) {
                    crateOutput += ChatColor.RED + crate + ChatColor.GOLD;
                    first = false;
                    continue;
                }
                crateOutput += ", " + ChatColor.RED + crate + ChatColor.GOLD;
            }

            sender.sendMessage(ChatColor.GOLD + "Crates: " + crateOutput);
            return true;
        }

        if (args[0].equals("deleteitem")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate deleteitem <tiername> <cratename> - Opens a gui to remove an item from the specified crate.");
                return true;
            }

            String tierName = args[1];
            String crateName = args[2];

            if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                return true;
            }

            if (Main.plugin.getConfig().get("tiers." + tierName + "." + crateName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
                return true;
            }

            List<ItemStack> items = (List<ItemStack>) Main.plugin.getConfig().getList("tiers." + tierName + "." + crateName);
            if (items.size() < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate has no items in it.");
                return true;
            }

            InventoryManager gui = new InventoryManager(ChatColor.GOLD + "Click an item to remove it", 27, Main.plugin);
            int invPos = 0;

            for (ItemStack curItem : items) {
                gui.withItem(invPos, curItem);
                invPos++;
            }

            gui.withEventHandler(new InventoryClickHandler() {
                @Override
                public void handle(InventoryClickEvent event) {
                    Player player1 = (Player) event.getWhoClicked();
                    ItemStack clicked = event.getCurrentItem();

                    if (clicked.getType().equals(Material.AIR)) {
                        event.setCancelled(true);
                        return;
                    }

                    InventoryManager confirmDelete = new InventoryManager(ChatColor.GOLD + "Confirm removal of " + ChatColor.RED + clicked.getType().toString(), 9, Main.plugin);

                    ItemStack yes = new ItemStack(Material.LIME_CONCRETE, 1);
                    ItemMeta yesMeta = yes.getItemMeta();
                    yesMeta.setDisplayName("Confirm");
                    yes.setItemMeta(yesMeta);

                    ItemStack no = new ItemStack(Material.RED_CONCRETE, 1);
                    ItemMeta noMeta = yes.getItemMeta();
                    noMeta.setDisplayName("Cancel");
                    no.setItemMeta(noMeta);

                    confirmDelete.withItem(0, yes);
                    confirmDelete.withItem(1, no);

                    confirmDelete.withEventHandler(new InventoryClickHandler() {
                        @Override
                        public void handle(InventoryClickEvent event1) {
                            Player player2 = (Player) event1.getWhoClicked();
                            ItemStack clicked1 = event1.getCurrentItem();
                            if (clicked1.getType().equals(Material.AIR)) {
                                event1.setCancelled(true);
                                return;
                            }

                            if (clicked1.getType().equals(Material.LIME_CONCRETE)) {
                                items.remove(clicked);
                                Main.plugin.getConfig().set("tiers." + tierName + "." + crateName, items);
                                Main.plugin.saveConfig();

                                player2.closeInventory();
                                player2.sendMessage(ChatColor.GOLD + "The item " + ChatColor.RED + clicked.getType().toString() + ChatColor.GOLD +
                                        " has been removed from the crate " + ChatColor.RED + crateName);
                            }

                            if (clicked1.getType().equals(Material.RED_CONCRETE)) {
                                player2.closeInventory();
                            }

                            confirmDelete.show(player1);
                        }
                    });
                }
            });

            gui.show(player);

            return true;

        }

        if (args[0].equals("create")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "/customcrate create tier <tiername> - Creates a tier.");
                sender.sendMessage(ChatColor.GOLD + "/customcrate create crate <cratename> <tiername> - Creates a crate in the specified tier.");
                return true;
            }
            if (args[1].equals("tier")) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate create tier <tiername> - Creates a tier.");
                    return true;
                }
                String tierName = args[2];
                if (Main.plugin.getConfig().get("tiers." + tierName) != null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A tier with that name already exists.");
                    return true;
                }

                Main.plugin.getConfig().set("tiers." + tierName, new ArrayList<>());
                Main.plugin.saveConfig();
                sender.sendMessage(ChatColor.GOLD + "A tier with that name has been created.");
                return true;
            }
            if (args[1].equals("crate")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate create crate <cratename> <tiername> - Creates a crate in the specified tier.");
                    return true;
                }

                String tierName = args[3];
                String crateName = args[2];

                if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                    return true;
                }

                if (Main.plugin.getConfig().get("tiers." + tierName + "." + crateName) != null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A crate with that name already exists.");
                    return true;
                }

                Main.plugin.getConfig().set("tiers." + tierName + "." + crateName, new ArrayList<ItemStack>());
                Main.plugin.saveConfig();

                sender.sendMessage(ChatColor.GOLD + "A crate with that name has been created.");
                return true;
            }
        }

        if (args[0].equals("add")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate add <tiername> <cratename> - Adds the item you're holding to the specified crate.");
                return true;
            }

            String tierName = args[1];
            String crateName = args[2];

            if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                return true;
            }

            if (Main.plugin.getConfig().get("tiers." + tierName + "." + crateName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
                return true;
            }

            ItemStack item = player.getInventory().getItemInMainHand();
            if (item.getType().equals(Material.AIR)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                return true;
            }

            List<ItemStack> items = (List<ItemStack>) Main.plugin.getConfig().getList("tiers." + tierName + "." + crateName);

            if (items.size() >= 27) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Crates may not contain more than 27 items.");
                return true;
            }

            items.add(item);

            Main.plugin.getConfig().set("tiers." + tierName  + "." + crateName, items);
            Main.plugin.saveConfig();

            sender.sendMessage(ChatColor.GOLD + "You've added an item to that crate.");
            return true;
        }

        if (args[0].equals("delete")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customcrate delete <tiername> <cratename>");
                return true;
            }

            String tierName = args[1];
            String crateName = args[2];

            if (Main.plugin.getConfig().get("tiers." + tierName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That tier does not exist.");
                return true;
            }

            if (Main.plugin.getConfig().get("tiers." + tierName + "." + crateName) == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
                return true;
            }

            Main.plugin.getConfig().set("tiers." + tierName + "." + crateName, null);
            Main.plugin.saveConfig();

            sender.sendMessage(ChatColor.GOLD + "That crate has been deleted.");
            return true;
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> tabComplete = new ArrayList<>();

        if (sender.hasPermission("customcrates.admin")) {
            if (args.length == 1) {
                tabComplete.add("create");
                tabComplete.add("listtiers");
                tabComplete.add("add");
                tabComplete.add("delete");
                tabComplete.add("deleteitem");
                tabComplete.add("drop");
            }
            if (args.length >= 2 && args[0].equals("create")) {
                if (args.length == 3 && args[1].equals("tier")) {
                    tabComplete.add("<tiername>");
                } else if (args.length == 3 && args[1].equals("crate")) {
                    tabComplete.add("<cratename>");
                } else {
                    tabComplete.add("tier");
                    tabComplete.add("crate");
                }
            }
            if (args.length >= 2 && args[0].equals("add")) {
                if (args.length == 3) {
                    tabComplete.add("<cratename>");
                } else {
                    tabComplete.add("<tiername>");
                }
            }
            if (args.length == 2 && args[0].equals("delete")) tabComplete.add("<tiername>");
            if (args.length >= 2 && args[0].equals("deleteitem")) {
                if (args.length == 3) {
                    tabComplete.add("<cratename>");
                } else {
                    tabComplete.add("<tiername>");
                }
            }
            if (args.length >= 2 && args[0].equals("drop")) {
                tabComplete.add("<tiername>");
                tabComplete.add("random");
            }
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
